import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.commons.logging.Log;

/*
 * Recommended Friends, MapReduce application 
 * 
 * @author Claudia M Ayala 
 * @author Edgar Morales Florez
 * @author Camilo Lozano
 * @version 1
 * @since 2015-03-10
 */

public class Friends {
	public static class Maper extends Mapper<LongWritable, Text, LongWritable, Text> {
		public static final Log LOG = LogFactory.getLog(Maper.class);
		public void map(LongWritable ikey, Text ivalue, Context context)
				throws IOException, InterruptedException {		
			String record[] = ivalue.toString().split("\t");
			//	LOG.info("Hola Mundo");
			if (record.length == 2) {
				Text friend2 = new Text();
				Text friend3 = new Text();
				Text friend4 = new Text();
				LongWritable idUser = new LongWritable(Long.parseLong(record[0]));			
				String friends[] = record[1].split(",");
				for (int i=0; i < friends.length; i++) {
					// Writes the triples of the form (id1, id2, -1). id3 = -1, because there is a link of friendship
					// id1 as key and id2, id3 as value
					friend2.set(friends[i]+","+"-1");
					context.write(idUser, friend2);
					//	LOG.info(idUser+","+friend2);
					// Writes the triples of the form (id1, id2, id3), id3 = mutual friend
					for (int j= i + 1; j < friends.length; j++) {
						friend3.set(friends[j]+","+idUser.toString());
						friend4.set(friends[i]+","+idUser.toString());
						context.write(new LongWritable(Long.parseLong(friends[i])),friend3);
						context.write(new LongWritable(Long.parseLong(friends[j])),friend4);
					}
				}
			}
		}
	}

	
	public static class Reduce extends Reducer<LongWritable, Text, LongWritable, Text> {
		public static final Log LOG = LogFactory.getLog(Reducer.class);
		public void reduce(LongWritable key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			// Is defined the maximum number of recommended friends
			int RECOMMEND_N = 10;
			String[] valueTemp;
			HashMap<Long, List<Long>> Friends = new HashMap<Long, List<Long>>();
			for (Text val : values) {
				valueTemp = (val.toString()).split(",");
				// Split the value in id2 and id3
				final Long Friend = Long.parseLong(valueTemp[0]);
				final Long friend2Way = Long.parseLong(valueTemp[1]);
				// Checks whether the id2 exists in the list 
				if (Friends.containsKey(Friend)) {
					// if id3 is a friend, write null to array of id2
					if (friend2Way == -1L) {	
						Friends.put(Friend, null); 
					} else if (Friends.get(Friend) != null) {
						// Or else is added id3 as mutual friend
						Friends.get(Friend).add(friend2Way);  
					}
				}
				else { 
					// if is not a friend, id2 is added as recommended friend and id3 as mutual friend
					if (friend2Way != -1L) {  
						Friends.put(Friend, new ArrayList<Long>() {
							{
								add(friend2Way);                              
							}
						});
					} else {
						Friends.put(Friend, null);
					}
				}
			}

			// Extracts only the recommended Friend and the number of mutual friends
			// and excludes id2 affected by a linkage of friendship
			final HashMap<Long, Integer> listeFQ = new HashMap<Long, Integer>();
			Iterator<Entry<Long, List<Long>>> it = Friends.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Long, List<Long>> e = (Entry<Long, List<Long>>)it.next();
				if (e.getValue() != null) {
					listeFQ.put(e.getKey(), e.getValue().size());
				}
			}

			// Convert listeFQ to list 
			ArrayList<Entry<Long, Integer>> list = new ArrayList<Entry<Long, Integer>>();
			for (Entry<Long, Integer> entry : listeFQ.entrySet()) {
				list.add(entry);
			}

			// Sorts the list in order of highest to lowest values of common friends.
			Collections.sort(list, new Comparator<Entry<Long, Integer>>() {
				public int compare(Entry<Long, Integer> idR1, Entry<Long, Integer> idR2) {
					return idR2.getValue().compareTo(idR1.getValue());
				}
			});

			// Output at most RECOMMEND_N keys with the highest values of mutual friends.
			ArrayList<Long> topN = new ArrayList<Long>();
			boolean equals = false;
			for (int i = 0; i < Math.min(RECOMMEND_N, list.size()); i++) {
				topN.add(list.get(i).getKey());
				// Checks for at least two recommended friends with the same number of mutual friends
				if ((Math.min(RECOMMEND_N, list.size()) >= 2) && (i < Math.min(RECOMMEND_N, list.size())-1)) {
					if ((list.get(i).getValue() == list.get(i+1).getValue())) {
						equals = true;
					}
				}
			}
			// Sorts the recommended list in order Ascending if id with the same number of mutual friends
			if (equals == true) {
				Collections.sort(topN);
			}
			// Writes the result
			context.write(key, new Text(StringUtils.join(topN, ",")));				
		}
	}


	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "Friends");
		job.setJarByClass(Friends.class);
		// TODO: specify a mapper
		job.setMapperClass(Maper.class);
		// TODO: specify a reducer
		job.setReducerClass(Reduce.class);

		// TODO: specify output types
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);
		
		FileSystem outFs = new Path(args[1]).getFileSystem(conf);
        outFs.delete(new Path(args[1]), true);

		// TODO: specify input and output DIRECTORIES (not files)
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		if (!job.waitForCompletion(true))
			return;
	}

}
